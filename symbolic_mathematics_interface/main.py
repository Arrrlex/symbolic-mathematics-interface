from typing import Optional
from pathlib import Path

from fastapi import FastAPI
from pydantic import BaseModel

from symbolicmathematics.integrate import Integrator

app = FastAPI()

base_path = Path(__file__).parent
model_base_path = base_path / 'models'
model_path = model_base_path / 'fwd_bwd_ibp.pth'

integrator = Integrator(cpu=True, model_path=str(model_path))

class Input(BaseModel):
    latex: str

class Result(BaseModel):
    latex: Optional[str]

@app.post('/integrate/')
def integrate(expr: Input) -> Result:
    input_latex = expr.latex
    output_latex = integrator.integrate(input_latex)
    return Result(latex=output_latex)
