FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8-slim

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1 \
    MODULE_NAME="symbolic_mathematics_interface" \
    PORT=$PORT

WORKDIR /app

RUN mkdir -p /usr/share/man/man1
RUN apt-get update && apt-get install -y default-jre antlr4 git

RUN pip install poetry

COPY pyproject.toml poetry.lock ./
RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-ansi --no-dev

COPY . ./

RUN pip uninstall --yes poetry
